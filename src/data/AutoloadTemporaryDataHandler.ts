export type AutoloadTemporaryDataHandler<T> = () => T | Promise<T>;
