export enum TemporaryDataEvent {
    EXPIRED = "expired",
    START = "start",
    STOP = "stop"
}
