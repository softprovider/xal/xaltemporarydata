//= Functions & Modules
// Own
import FlexibleTimeoutTemporaryData from './FlexibleTimeoutTemporaryData';

//= Structures & Data
// Own
import { AutoloadMethod } from '../data/AutoloadMethod';
import { AutoloadTemporaryDataHandler } from '../data/AutoloadTemporaryDataHandler';
import { TemporaryDataEvent } from '../data/TemporaryDataEvent';

export default class AutoloadFlexibleTimeoutTemporaryData<T> extends FlexibleTimeoutTemporaryData<T> {
    protected method: AutoloadMethod = AutoloadMethod.ON_TIMEOUT;
    protected autoloadHandler: AutoloadTemporaryDataHandler<T>;

    constructor(milliseconds?: number, method?: AutoloadMethod, autoloadHandler?: AutoloadTemporaryDataHandler<T>) {
        super(null, milliseconds, false, true);

        if (method != null) this.autoloadMethod(method);
        if (autoloadHandler) this.setAutoloadHandler(autoloadHandler); 
    }

    public async getData(): Promise<T> {
        if (super.getData() != null) {
            if (this.renewOnGet) {
                await this.loadData();
                return this.data;
            }

            return this.data;
        } else if (this.method == AutoloadMethod.ON_ACCESS) {
            await this.start(null, true);
            return this.data;
        } else {
            return null;
        }
    }

    public async start(milliseconds?: number, loadData?: boolean) {
        super.start(milliseconds);
        if (loadData || this.method == AutoloadMethod.ON_TIMEOUT) await this.loadData();
    }

    public async loadData() {
        super.setData(await this.autoloadHandler(), false);
    }

    public setAutoloadHandler(autoloadHandler: AutoloadTemporaryDataHandler<T>) {
        this.autoloadHandler = autoloadHandler;
    };

    public autoloadMethod(method: AutoloadMethod) {
        this.method = method;

        if (this.method == AutoloadMethod.ON_TIMEOUT) {
            this.on(TemporaryDataEvent.EXPIRED, this.loadData.bind(this));
        } else {
            this.off(TemporaryDataEvent.EXPIRED, this.loadData.bind(this));
        }
    }

}
