//= Functions & Modules
// Own
import TimeoutTemporaryDataBase from './TimeoutTemporaryDataBase';

export default class FixedTimeoutTemporaryData<T> extends TimeoutTemporaryDataBase<T> {
    protected timerID: any;
    protected renewOnGet: boolean;

    public getData(): T | Promise<T> {
        if (this.renewOnGet) {
            this.stopTimer();
            this.startTimer();
        }

        return this.data;
    }

    public start(milliseconds?: number) {
        super.start(milliseconds);
        this.startTimer();
    }

    public stop() {
        super.stop();
        this.stopTimer();
    }

    protected startTimer() {
        this.timerID = setTimeout(this.onTimeout.bind(this), this.milliseconds);
    }

    protected stopTimer() {
        clearTimeout(this.timerID);
    }

    protected onTimeout() {
        this.makeExpired();
    }
}
