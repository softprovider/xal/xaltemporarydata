//= Functions & Modules
// Own
import TimeoutTemporaryDataBase from './TimeoutTemporaryDataBase';

export default class FlexibleTimeoutTemporaryData<T> extends TimeoutTemporaryDataBase<T> {
    protected startedDate: number;
    protected expireDate: number;

    public hasExpired(): boolean {
        return Date.now() > this.expireDate;
    }

    public getData(): T | Promise<T> {
        if (this.renewOnGet) {
            this.start();
        } else if (Date.now() > this.expireDate) {
            super.makeExpired();
        }

        return this.data;
    }

    public start(milliseconds?: number) {
        super.start(milliseconds);
        this.startedDate = Date.now();
        this.setExpireDate();
    }

    protected setExpireDate() {
        this.expireDate = this.startedDate + this.milliseconds;
    }

    public getExpireDate(): number {
        return this.expireDate;
    }

    public getStartDate(): number {
        return this.startedDate;
    }

    public extendTime(milliseconds: number) {
        this.expireDate += milliseconds;
    }

    public setRenewOnGet(renew: boolean) {
        super.setRenewOnGet(renew);
    }
}

