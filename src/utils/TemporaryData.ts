//= Functions & Modules
// Others
import { EventEmitter } from 'events';

//= Structures & Data
// Own
import { TemporaryDataEvent } from '../data/TemporaryDataEvent';

export default abstract class TemporaryData<T> extends EventEmitter {
    protected data: T;
    protected started: boolean;
    protected expired: boolean;
    protected eventsEnabled: boolean;

    constructor(data?: T, eventsEnabled: boolean = false) {
        super();
        this.data = data;

        this.eventsEnabled = eventsEnabled;
    }

    public setEventsEnabled(eventsEnabled: boolean) {
        this.eventsEnabled = eventsEnabled;
    }

    public hasStarted(): boolean {
        return this.started;
    }

    public hasExpired(): boolean {
        return this.expired;
    }

    public makeExpired(): void {
        this.invalidateData();
        this.stop();
        this.expired = true;

        if (this.eventsEnabled) this.emit(TemporaryDataEvent.EXPIRED);
    }

    public setData(data: T) {
        this.data = data;
    }

    public getData(): T | Promise<T> {
        return this.data;
    }

    public start() {
        this.started = true;
        this.expired = false;

        if (this.eventsEnabled) this.emit(TemporaryDataEvent.START);
    }

    public stop() {
        this.started = false;

        if (this.eventsEnabled) this.emit(TemporaryDataEvent.STOP);
    }

    public reset() {
        this.stop();
        this.start();
    }

    public invalidateData() {
        this.data = null;
    }
}
