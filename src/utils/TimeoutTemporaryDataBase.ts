//= Functions & Modules
// Own
import TemporaryData from './TemporaryData';

export default class TimeoutTemporaryDataBase<T> extends TemporaryData<T> {
    protected milliseconds: number;
    protected renewOnGet: boolean;

    constructor(data?: T, milliseconds?: number, start?: boolean, eventsEnabled: boolean = true) {
        super(data, eventsEnabled);

        if (milliseconds) this.setTimeoutMilliseconds(milliseconds);
        if (start) this.start();
    }

    public setData(data: T, start?: boolean): void {
        this.data = data;

        if (start && this.milliseconds) this.start();
    }

    public start(milliseconds?: number) {
        if (milliseconds) this.milliseconds = milliseconds;
        super.start();
    }

    public setTimeoutMilliseconds(milliseconds: number) {
        this.milliseconds = milliseconds;
    }

    public setRenewOnGet(renew: boolean) {
        this.renewOnGet = renew;
    }
}

